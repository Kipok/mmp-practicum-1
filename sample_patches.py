import numpy as np
from numpy.lib.stride_tricks import as_strided


def normalize_data(images):
    mean = np.mean(images, 1)[:,np.newaxis]
    std = np.std(images, 1)[:,np.newaxis]

    images = np.maximum(np.minimum(images, mean + 3 * std), mean - 3 * std)
    images = (images - np.min(images, 1)[:,np.newaxis])
    images = images / (np.max(images, 1)[:,np.newaxis] + 1e-7) * 0.8 + 0.1

    return images


def sample_patches_raw(images, num_patches=10000, patch_size=8):
    img_size = (96, 96, 3)
    arr = images.reshape(images.shape[0], *img_size)
    rand_imgs = np.random.randint(0, arr.shape[0], num_patches)
    rand_ind1 = np.random.randint(0, arr.shape[1] - patch_size, num_patches)
    rand_ind2 = np.random.randint(0, arr.shape[2] - patch_size, num_patches)

    out = np.empty((num_patches, 3 * patch_size ** 2), dtype=arr.dtype)

    for i in range(num_patches):
        out[i] = arr[rand_imgs[i], rand_ind1[i] : rand_ind1[i] + patch_size, rand_ind2[i] : rand_ind2[i] + patch_size, :].reshape(-1)

    return out


def sample_patches(images, num_patches=10000, patch_size=8):
    return normalize_data(sample_patches_raw(images, num_patches, patch_size))


def patchify_images(images, patch_size=8, patch_stride=4):
    img_size = (96, 96, 3)
    assert((img_size[0] - patch_size) % patch_stride == 0)

    patch_shape = (patch_size, patch_size, 3)
    patch_stride = np.array((1, patch_stride, patch_stride, 1))
    arr = images.reshape(images.shape[0], *img_size)

    shape = tuple(list((np.array(arr.shape) - np.array((1,) + patch_shape)) / patch_stride + 1) + list(patch_shape))
    strides = (arr.strides[0],arr.strides[1]*patch_stride[1],arr.strides[2]*patch_stride[1],arr.strides[3], arr.strides[1],arr.strides[2],arr.strides[3])
    #print(strides)
    patches = as_strided(arr, shape=shape, strides=strides)

    tmp = patches.reshape((-1,) + patch_shape)

    return normalize_data(tmp.reshape(tmp.shape[0], -1))
