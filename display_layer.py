import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import imsave
from scipy.misc import imread


def display_layer(x, filename='layer.png'):
    assert(int(np.sqrt(x.shape[0])) * int(np.sqrt(x.shape[0])) == x.shape[0])

    sz1, sz2 = int(np.sqrt(x.shape[0])), int(np.sqrt(x.shape[1] / 3))
    xr = x.reshape(sz1, sz1, sz2, sz2, 3)
    sz2 += 2
    xn = np.empty((sz1*sz2, sz1*sz2, 3), dtype=x.dtype)

    for i in range(sz1):
        for j in range(sz1):
            xn[i*sz2:(i + 1)*sz2,j*sz2:(j + 1)*sz2] = np.lib.pad(xr[i,j], ((1,1),(1,1),(0,0)), 'constant', constant_values=0)

    imsave(filename, xn)

    tmp = imread(filename)
    plt.axis('off')
    plt.imshow(tmp)
