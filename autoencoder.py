import numpy as np

#pylint: skip-file

class SimpleNet:
    def __init__(self, rho=0.01, l=0.0001, beta=3):
        self.layers = [None] * 2
        self.layers[0] = Layer(192, 75)
        self.layers[1] = Layer(75, 192)
        self.rho = rho
        self.l = l
        self.beta = beta
        self.num_weights = 0
        for layer in self.layers:
            self.num_weights += layer.num_weights

    def compute_net(self, x):
        input = x
        for layer in self.layers:
            output = layer.compute(input)
            input = output
        return output

    def compute_grad(self, x):
        grads = np.empty(self.num_weights)
        idx = self.num_weights
        out = self.compute_net(x)
        obj = np.sum((out - x) ** 2) / (2 * x.shape[0])
        dLda = (out - x)
        for i in range(len(self.layers) - 1, -1, -1):
            idx -= self.layers[i].num_weights
            if i != len(self.layers) - 1:
                dLdw, dLdx = self.layers[i].backprop(dLda, self.l, self.rho, self.beta)
                obj += self.beta * np.sum(self.rho * np.log(self.rho / self.layers[i].rho_j) +
                                                (1 - self.rho)*np.log((1 - self.rho) / (1 - self.layers[i].rho_j)))
            else:
                dLdw, dLdx = self.layers[i].backprop(dLda, self.l, self.rho, 0)
            grads[idx:idx + self.layers[i].num_weights] = dLdw
            dLda = dLdx
            obj += self.l / 2 * np.linalg.norm(self.layers[i].w)
        return obj, grads


    def unfold(self):
        weights = np.empty(self.num_weights)
        idx = 0
        for layer in self.layers:
            weights[idx:idx + layer.num_weights] = layer.get_weights()
            idx += layer.num_weights
        return weights

    def fold(self, weights):
        idx = 0
        for layer in self.layers:
            layer.set_weights(weights[idx:idx + layer.num_weights])
            idx += layer.num_weights

    def fit_traindata(self, data):
        self.train_data = data

    def obj_and_grad(self, weights):
        self.fold(weights)
        return self.compute_grad(self.train_data)

    def get_layer_output(self, k, x):
        """
        k -- number of the layer from 1 to num_layers
        For example for simple net with 1 hidden layer get_layer_output(1, x) will return the output of the hidden layer
        get_layer_output(2, x) will return the output of the net
        """
        input = x
        i = 0
        for layer in self.layers:
            if i == k:
                break
            output = layer.compute(input)
            input = output
            i += 1
        return output

    def get_max_activation(self):
        center = len(self.layers) // 2 - 1
        norms = np.apply_along_axis(np.linalg.norm, 1, self.layers[center].w.T)
        tmp = self.layers[center].w.T / norms[:,np.newaxis]
        return (tmp - np.min(tmp, 1)[:,np.newaxis]) / (np.max(tmp, 1)[:,np.newaxis]- np.min(tmp, 1)[:,np.newaxis] + 1e-7)


class ThreeLayeredNet(SimpleNet):
    def __init__(self, rho=0.01, l=0.0001, beta=3):
        self.layers = [None] * 4
        self.layers[0] = Layer(192, 90)
        self.layers[1] = Layer(90, 75)
        self.layers[2] = Layer(75, 90)
        self.layers[3] = Layer(90, 192)
        self.rho = rho
        self.l = l
        self.beta = beta
        self.num_weights = 0
        for layer in self.layers:
            self.num_weights += layer.num_weights


class Layer:
    def __init__(self, n_in, n_out):
        magic_num = np.sqrt(6 / (n_in + n_out + 1))
        self.w = np.random.uniform(-magic_num, magic_num, (n_in, n_out))
        self.b = np.zeros(n_out)
        self.num_weights = (n_in + 1) * n_out
        self.avg_act = 0

    def activation(self, x):
        return 1 / (1 + np.exp(-x))

    def compute(self, x):
        self.act = self.activation(x.dot(self.w) + self.b)
        self.rho_j = np.sum(self.act, axis=0) / self.act.shape[0]
        self.x = x
        return self.act

    def backprop(self, dLda, l, rho, beta):
        dLda += beta * (-rho / self.rho_j + (1 - rho) / (1 - self.rho_j))
        dLda *= self.act * (1 - self.act)
        dLdx = dLda.dot(self.w.T)
        dLdw = self.x.T.dot(dLda) / self.x.shape[0] + self.w * l
        dLdb = np.sum(dLda, axis=0) / self.x.shape[0]
        return np.hstack((dLdw.reshape(-1), dLdb)), dLdx

    def get_weights(self):
        return np.hstack((self.w.reshape(-1), self.b))

    def set_weights(self, weights):
        self.b = weights[-self.b.shape[0]:]
        self.w = weights[:-self.b.shape[0]].reshape(self.w.shape)

class ReluLayer(Layer):
    def activation(self, x):
        return np.maximum(0, x)

    def backprop(self, dLda, l, rho, beta):
        eps = 1e-7
        dLda += beta * (-rho / self.rho_j + (1 - rho) / (1 - self.rho_j))
        dLda[dLda < eps] = 0
        dLdx = dLda.dot(self.w.T)
        dLdw = self.x.T.dot(dLda) / self.x.shape[0] + self.w * l
        dLdb = np.sum(dLda, axis=0) / self.x.shape[0]
        return np.hstack((dLdw.reshape(-1), dLdb)), dLdx

    def compute(self, x):
        self.act = self.activation(x.dot(self.w) + self.b)
        self.rho_j = np.sum(self.act, axis=0) / self.act.shape[0] + 1e-7
        self.x = x
        return self.act

class ReluNet(SimpleNet):
    def __init__(self, rho=0.01, l=0.0001, beta=3):
        self.layers = [None] * 2
        self.layers[0] = ReluLayer(192, 75)
        self.layers[1] = ReluLayer(75, 192)
        self.rho = rho
        self.l = l
        self.beta = beta
        self.num_weights = 0
        for layer in self.layers:
            self.num_weights += layer.num_weights
