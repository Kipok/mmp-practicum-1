import numpy as np
import sys

def compute_gradient(J, theta):
    eps = 1e-7
    grad = np.empty(theta.shape)[:200]
    for i in range(theta.shape[0]):
        if i == 200:
            break # I'm tired of waiting..
        e_i = np.zeros(theta.shape)
        e_i[i] = 1.0
        grad[i] = (J(theta + eps * e_i) - J(theta - eps * e_i)) / (2 * eps)
    return grad


def check_gradient():
    simple_func = lambda theta : 3 * theta.dot(theta.T) + 5 * np.log(theta[0])
    grad = lambda theta : 6 * theta + 5 * np.eye(1, theta.shape[0]).squeeze() / theta

    theta = np.random.rand(200)

    my_gr = compute_gradient(simple_func, theta)
    real_gr = grad(theta)

    np.testing.assert_array_almost_equal(my_gr, real_gr)
